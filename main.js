import Vue from 'vue'
import App from './App'

import {http} from './service/index.js'
Vue.prototype.$http = http

// 封装提示组件
uni.$showMsg = function(title = '数据请求失败', duration = 1500) {
	uni.showToast({
		title,
		duration,
		icon: "none"
	})
}

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
