/**
 * 首页api
 */
import {http} from '../index.js'


/**
 * 轮播图
 * @returns {Promise}
 */
export function getSwrperList(data){
	let config = {}
	let e = http.get("/api/public/v1/home/swiperdata", "");
    return e
}

/**
 * 分类导航
 * @returns {Promise}
 */
export function getNavList(data){
	let config = {}
	let e = http.get("/api/public/v1/home/catitems", "");
    return e
}



/**
 * 获取首页楼层数据
 * @returns {Promise}
 */
export function geFloorList(data){
	let config = {}
	let e = http.get("/api/public/v1/home/floordata", "");
    return e
}

