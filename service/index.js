import Vue from 'vue'
import Request from './request.js'
const http = new Request();
// 此方法在request.js里用于更改基础请求路径
http.setConfig((config) => {
	var url=""
	if(process.env.NODE_ENV === 'development'){
		url = 'https://api-hmugo-web.itheima.net';
	}else{
		url = '';
	}
	config.baseUrl = url
	
	var _token = uni.getStorageSync('setUserToken'); // 从本地缓存中同步获取指定 key 对应的内容。
	if (_token) {
		var token = {};
		if (typeof _token === 'string') {
			token = JSON.parse(_token);
		} else {
			token = _token;
		}
		if (token) {
			config.header.Authorization = token.token_type + ' ' + token.auth_token;
		}
	}
	config.header.Authorization = '11'
	return config
})

http.interceptor.request((config, cancel) => {
	/* 请求之前拦截器 */
	uni.showLoading({
		title: "请稍后...",
		mask: true
	})
	return config;
})
http.interceptor.response(
	(response) => {
		/* 请求之后拦截器 */
		uni.hideLoading(); //关闭加载动画
		if (!response || response.statusCode == 401) {
			uni.clearStorage();
			uni.reLaunch({
				url: '/pages_package_login/login/login.vue',
			});
			return false;
		} 
		if (!response || response.statusCode == 404) {
			uni.showToast({
				icon: 'none',
				position: 'bottom',
				title: '接口不存在'
			});
		} 
		else if (response.errMsg && response.errMsg == 'request:fail') {
			uni.showToast({
				icon: 'none',
				position: 'bottom',
				title: '网络连接失败'
			});
		} else {
			if (response.statusCode == 200) {
				return response.data;
			} else {
				if (!response.data) {
					uni.reLaunch({
						url: '/pages_package_login/login/login.vue',
					});
				} else if (response.data.statusCode == 402) {
					uni.showModal({
						title: '提示',
						content: response.data.msg,
						showCancel: false
					});
				} else if (response.data.statusCode == 409) {
					uni.showModal({
						title: '提示',
						content: response.data.msg,
						showCancel: false
					});
				} else if (response.data.statusCode == 400) {
					uni.showToast({
						icon: 'none',
						position: 'bottom',
						title: '非法操作'
					});
				} else {
					uni.showToast({
						icon: 'none',
						position: 'bottom',
						title: '系统错误'
					});
				}
			}
		}
	
	})
export {
	http
};

