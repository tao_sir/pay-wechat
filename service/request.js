export default class Request {
	config = {
		baseUrl: '',
		header: {
			'Content-Type': 'application/json;charset=UTF-8'
		},
		method: 'GET',
		dataType: 'json',
		responseType: 'text',
		success() {},
		fail() {},
		complete() {}
	}

	static posUrl(url) { /* 判断url是否为绝对路径 */
		return /(http|https):\/\/([\w.]+\/?)\S*/.test(url)
	}

	interceptor = {
		request(f) {
			if (f) {
				Request.requestBeforeFun = f
			}
		},
		response(f) {
			if (f) {
				Request.requestComFun = f
			}
		}
	}

	static requestBeforeFun(config) {
		return config
	}

	static requestComFun(response) {
		return response
	}
	// 更改基础请求路径
	setConfig(f) {
		this.config = f(this.config)
	}

	request(options = {}) {
		options.baseUrl = options.baseUrl || this.config.baseUrl
		options.dataType = options.dataType || this.config.dataType
		options.url = Request.posUrl(options.url) ? options.url : (options.baseUrl + options.url)
		options.data = options.data || {}
		options.header = options.header || this.config.header
		options.method = options.method || this.config.method
		this.checkLogin(options);
		return new Promise((resolve, reject) => {
			let next = true
			let _config = null
			options.complete = (response) => {
				let statusCode = response.statusCode
				response.config = _config
				response = Request.requestComFun(response)
				if (statusCode === 200) { // 成功
					resolve(response)
				} else {
					if (response)
						reject(response)
					else
						reject(null);
				}
			}
			let cancel = (t = 'handle cancel') => {
				let err = {
					errMsg: t,
					config: afC
				}
				reject(err)
				next = false
			}
			let afC = { ...this.config,
				...options
			}
			_config = { ...afC,
				...Request.requestBeforeFun(afC, cancel)
			}
			if (!next) return
			uni.request(_config)
		})
	}
	
	// 设置 get 和 post 请求 需要url请求地址 data所需参数 
	get(url, data, options = {}) {
		options.url = url
		options.data = data
		options.method = 'GET'
		// 使用 this.request 发起请求，传入参数，获取数据
		return this.request(options)
	}

	post(url, data, options = {}) {
		options.url = url
		options.data = data
		options.method = 'POST'
		return this.request(options)
	}
	
	checkLogin(options) {
		if (!options.url.includes("/login") && !options.header.Authorization) {
			if (true) {
				uni.showModal({
					title: '提示',
					content: '您还没有登录，是否进行授权登录？',
					success: function(res) {
						if (res.confirm) {
							uni.switchTab({
								url: '/pages/my/my', //授权页面
							})
						} else if (res.cancel) {
							uni.showModal({
								title: '温馨提示',
								content: '拒绝后将会对您的功能体验有部分影响',
								confirmText: "知道了",
								showCancel: false,
								success: function(res) {
									///点击知道了的后续操作 
									///如跳转首页面 
								}
							});
						}
					}
				});
				throw ('暂未登录,已阻止此次API请求~');
			}
		}
	}
}
